#!/usr/bin/env bash
set -euo pipefail
script=$(readlink -f "${BASH_SOURCE[0]}")
pypette=$(readlink -f $(dirname $(dirname "${BASH_SOURCE[0]}")))
source "${pypette}/bin/src-export.sh"
source pypette.sh

trap ${pypette}/dev/lock-project SIGKILL ERR

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#                             WARNING 
#  
# Hard Reset on the current branch is done here. 
# Make sure you have all your modifications stashed BEFORE execution!
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

## Unlock
${pypette}/dev/unlock-project

## Cancel all file modifications
git checkout -- $(pypette::lockableFiles)

## Update Repo
git fetch --all
printf "Current branch: '$(pypette::currentBranch)'\n" >&2
printf "Hard reset from: '$(pypette::originBranch)'\n" >&2
git reset --hard $(pypette::originBranch)

## Update Submodules
printf "Updating Submodules.\n" >&2
git submodule init
git submodule update

## Set Stats Files
pypette::checkUsageFile

## Set Permissions
${pypette}/dev/lock-project
chmod 777 "$(pypette::usageFile)"

## Restrict Deployment Execution
chmod -R g-x,o-x "$script" 
